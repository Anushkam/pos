﻿using Codezync.Pos.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.ViewModels
{
    public class ItemCartViewModel
    {
        [Key]
        public int Id { get; set; }
        public string SessionId { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public ProductViewModel Product { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public CustomerViewModel Customer { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }

        //[ForeignKey("CashPaymentReference")]
        public int CashPaymentReferenceId { get; set; }
        //public CashPaymentReferenceViewModel CashPaymentReference { get; set; }
        //[ForeignKey(" CardPaymentReference")]
        public int CardPaymentReferenceId { get; set; }
        //public CardPaymentReferenceViewModel CardPaymentReference { get; set; }

    }

    public class CartSummeryViewModel
    {
       
        public string SessionId { get; set; }
        public double TotalAmount { get; set; }
        public int TotalQuantity { get; set; }
        public double? TotalAfterDiscounts { get; set; }
        public int CustomerId { get; set; }
        public PaymentType PaymentType { get; set; }
        public double? DiscountRate { get; set; }


    }
}
