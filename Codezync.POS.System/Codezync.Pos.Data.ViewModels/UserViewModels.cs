﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Codezync.Pos.Data.ViewModels
{
    public class EditUserViewModel : UserViewModel
    {
        public IEnumerable<RolesViewModel> RolesList { get; set; }

        public IEnumerable<RolesViewModel> UserRoles { get; set; }
        public PostedRoles PostedRoles { get; set; }
    }
    public class RolesViewModel
    {
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class PostedRoles
    {
        public string[] RoleList { get; set; }
    }
    public class UserViewModel
    {
        public string Id { get; set; }

        [Display(Name = "User Name")]
        [Required(AllowEmptyStrings = false)]
        public string UserName { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
               
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        public string[] Roles { get; set; }

        public IEnumerable<string> ListOfRoles { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set; }
    }

    public class UserFilterCriteria
    {
        public string SearchString { get; set; }

        public string SearchBy { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }
    }

}
