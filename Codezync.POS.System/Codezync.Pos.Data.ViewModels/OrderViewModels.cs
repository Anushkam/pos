﻿using Codezync.Pos.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.ViewModels
{
    
    public class OrderViewModel
    {
        [Key]
        public int Id { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string CashierId { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public CustomerViewModel Customer { get; set; }

        public PaymentType PaymentType { get; set; }

        public double Amount { get; set; }
        public double DiscountRate { get; set; }
        public double TotalWithDiscounts { get; set; }
        public double AmountPaid { get; set; }
        public double Cost { get; set; }
        public ICollection<OrderItemViewModel> OrderItems { get; set; }
    }

    public class OrderItemViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public OrderViewModel Order { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}
