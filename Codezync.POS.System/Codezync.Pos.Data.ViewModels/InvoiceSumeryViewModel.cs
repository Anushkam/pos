﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.ViewModels
{
    public class InvoiceSumeryViewModel
    {
        public int OrderId { get; set; }
        
        public double TotalAmount { get; set; }
        public double SubTotal { get; set; }

        public double Paid { get; set; }
        public double DiscountRate { get; set; }
        public DateTime Date { get; set; }

        public CustomerViewModel Customer { get; set; }

        public IEnumerable<OrderItemViewModel> OrderedItems { get; set; }
    }
}
