﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.ViewModels
{
    public enum PaymentTypeViewModel
    {
        Cash = 0,
        Card = 1
    }

    public class CardPaymentReferenceViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string TransactionId { get; set; }
        public double Amount { get; set; }
        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public CustomerViewModel Customer { get; set; }
        public string CardReference { get; set; }

    }

    public class CashPaymentReferenceViewModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public double Tendered { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public CustomerViewModel Customer { get; set; }
    }
}
