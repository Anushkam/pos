﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.ViewModels
{
    public class ProductViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public double Price { get; set; }
        public decimal Cost { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        
        [ForeignKey("Unit")]
        public int? UnitId { get; set; }
        public UnitViewModel Unit { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public ICollection<ProductCategoryViewModel> ProductCategories { get; set; }
        public int[] SelectedCategoryIds { get; set; }
    }

    public class ProductCategoryViewModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
        [DisplayName("Category Image")]
        public string CategoryImgeUrl { get; set; }
        [DisplayName("Is Active")]
        public bool IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public ICollection<ProductViewModel> Products { get; set; }

    }

    public class ProductCriteria
    {
        public string SearchBy { get; set; }
        public string SearchString { get; set; }
        public int ProductId { get; set; }
        public int ProductCategoryId { get; set; }

    }
}
