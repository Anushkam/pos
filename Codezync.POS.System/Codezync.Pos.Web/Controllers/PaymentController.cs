﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;

namespace Codezync.Pos.Web.Controllers
{
    public class PaymentController : BaseController
    {
        private OrderService orderService;
        private CartService cartService;
        private PaymentService paymentService;
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            orderService = new OrderService(CZDBContext);
            cartService = new CartService(CZDBContext);
            paymentService = new PaymentService(CZDBContext);
        }
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// to identify previously selected payment type
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCurrentPaymentType()
        {
            if (Session["CART_ID"] != null)
            {
                return Json(cartService.GetCurrentPaymentType(Session["CART_ID"].ToString()), JsonRequestBehavior.AllowGet);
            }

            var getPaymentType = cartService.GetCurrentPaymentType();
            return Json(getPaymentType, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// get curret cart data to show when landing to payment control
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCurrentCartData()
        {
            if (Session["CART_ID"] != null)
            {
                var summery = cartService.GetCartSummery(Session["CART_ID"].ToString());
                return Json(new { total = summery.TotalAmount, customerId = summery.CustomerId }, JsonRequestBehavior.AllowGet);
            }

            var _summery = cartService.GetCartSummery();
            return Json(new { total = _summery.TotalAmount, customerId = _summery.CustomerId }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// save cash payment when selected payment type as cash
        /// </summary>
        /// <param name="paidAmount"></param>
        /// <param name="total"></param>
        /// <param name="customerId"></param>
        /// <param name="discountRate"></param>
        /// <returns></returns>
        public JsonResult SaveAsCashPayment(double paidAmount, double total, int customerId, double discountRate)
        {
            var paymentId = paymentService.AddCashPayment(paidAmount, total, customerId);
            if (paymentId != 0)
            {
                cartService.AddCashPayment(paymentId, discountRate);
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// save card payments when card selected as payment type
        /// </summary>
        /// <param name="cardReference"></param>
        /// <param name="total"></param>
        /// <param name="customerId"></param>
        /// <param name="discountRate"></param>
        /// <returns></returns>
        public JsonResult SaveAsCardPayment(string cardReference, double total, int customerId, double discountRate)
        {
            var paymentId = paymentService.AddCardPayment(cardReference, total, customerId);
            if (paymentId != 0)
            {
                cartService.AddCardPayment(paymentId, discountRate);
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Here the order will be permanently saved in the dbb and will show the
        /// invoice and clear the session for current order.
        /// </summary>
        /// <returns></returns>
        public ActionResult Invoice()
        {
            if (Session["CART_ID"] != null)
            {
                var CashierId = User.Identity.GetUserId();
                var orderId = orderService.SaveAnOrder(CashierId, Session["CART_ID"].ToString());
                var summery = orderService.GetInvoiceSummery(orderId);

                cartService.ClearCart(Session["CART_ID"].ToString());
                Session["CART_ID"] = null;
                

                return View(summery);
            }
            return RedirectToAction("Index","POS");
        }


    }
}