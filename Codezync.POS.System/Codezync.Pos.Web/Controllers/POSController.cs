﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Codezync.Pos.Web.Controllers
{
    public class POSController : BaseController
    {

        private ProductService productService;
        private ProductCategoryService productCategoryService;
        private OrderService orderService;
        private CartService cartService;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            productService = new ProductService(CZDBContext);
            productCategoryService = new ProductCategoryService(CZDBContext);
            orderService = new OrderService(CZDBContext);
            cartService = new CartService(CZDBContext);
            

        }
        // GET: POS
        public ActionResult Index()
        {
            var getCartList = cartService.GetCartItems();
            if (getCartList.Count() == 0)
            {
                Session["CART_ID"] = Guid.NewGuid();

            }
            else {
                Session["CART_ID"] = getCartList.FirstOrDefault().SessionId;
            }
            ViewBag.SelectedCustomer = cartService.GetSelectedCustomer(Session["CART_ID"].ToString()).Name;
            return View();
        }

        public PartialViewResult Categories()
        {
            var categories = productCategoryService.GetProductCategories();
            return PartialView("_Categories", categories);
        }
        public ActionResult Recipt()
        {
            return View();
        }
        public ActionResult Payments()
        {
            return View();
        }

        
        public ActionResult ClearCart()
        {
            if (Session["CART_ID"] != null)
            {
                cartService.ClearCart(Session["CART_ID"].ToString());
                Session["CART_ID"] = null;
                var cartItems = cartService.GetCartItems();
                return PartialView("_CartList", cartItems);
            }
            return View();
        }

        //
        public ActionResult LoadSelectedProducts(int catId = 0)
        {
            var products = productCategoryService.GetProductsByCategoryId(catId);
            return PartialView("_loadProductList", products);
        }

        public ActionResult CartSummery()
        {
            if (Session["CART_ID"] != null) {
                var summery = cartService.GetCartSummery(Session["CART_ID"].ToString());
                return PartialView("_CartSummery", summery);
            }
            return PartialView("_CartSummery", new CartSummeryViewModel());
        }

        public ActionResult CartList()
        {           
            var cartItems = cartService.GetCartItems();
            return PartialView("_CartList", cartItems);
        }

        public ActionResult RemoveFromCartList(int productItemId)
        {
            cartService.RemoveItemFromCart(productItemId);
            var cartItems = cartService.GetCartItems();
            return PartialView("_CartList", cartItems);
        }
        //public ActionResult ClearCart(int productItemId)
        //{
        //    cartService.ClearCart(Session["CART_ID"].ToString());
        //    var cartItems = cartService.GetCartItems();
        //    return PartialView("_CartList", cartItems);
        //}
        public ActionResult AddToCartList(ItemCartViewModel vm)
        {
            //New session will be created if a new order
            //Session should be cleared when save the order to db
            if (Session["CART_ID"] == null)
            {
                Session["CART_ID"] = Guid.NewGuid();
                
            }
            vm.SessionId = Session["CART_ID"].ToString();
            cartService.AddtoCart(vm);

            var cartItems = cartService.GetCartItems();
            return PartialView("_CartList", cartItems);
        }

        public PartialViewResult SearchProducts(int catId = 0, string searchKey = "")
        {
            
            var products = productService.GetProdcts(searchKey, catId);
            return PartialView("_loadProductList", products);

        }
    }
}