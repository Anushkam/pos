﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.Service;
using System.Web.Routing;
using Codezync.Pos.Data.ViewModels;

namespace Codezync.Pos.Web.Controllers
{
    [Authorize(Roles = "Administrator, Admin, SuperUser")]
    public class HomeController : AdminController
    {

        private ProductService productService;
        private ProductCategoryService productCategoryService;
        private OrderService orderService;
        private CartService cartService;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            productService = new ProductService(CZDBContext);
            productCategoryService = new ProductCategoryService(CZDBContext);
            orderService = new OrderService(CZDBContext);
            cartService = new CartService(CZDBContext);

        }

        public HomeController()
        {
        }
        public ActionResult Index()
        {
            // var test = productService.GetProductByCode("");
            //var tesmp = productCategoryService.GetProductCategoryByCode("FOODS");
            //var order = orderService.GetOrderById(3);
            //var orderItems = orderService.GetOrderItemsByOrderId(3);
            //ViewBag.SESSION = "adad";
            //if (Session["CART"] != null)
            //{
            //    if (Session["CART"].ToString() == "NEW")
            //    {
            //        ViewBag.SESSION = "New";
            //    }
            //    else
            //    {
            //        ViewBag.SESSION = "Old";
            //    }

            //}

            //var itemCartVm = new ItemCartViewModel {
            //        SessionId = "1234",
            //        ProductId = 1,
            //        Quantity = 1,
            //        Price = 120                    
            //};

            //cartService.AddtoCart(itemCartVm);

            //var cartItesm = cartService.GetCartItems("1234");
            ViewBag.Session = Session["CART_ID"];


            return View();
        }

        public ActionResult SetSession()
        {
            Session["CART"] = "NEW";

            //cartService.RemoveItemFromCart(1);
            cartService.ClearCart("1234");

            return Redirect("Index");
        }
        public ActionResult GetSession()
        {

            Session["CART"] = "NOT_NEW";
            var itemCartVm = new ItemCartViewModel
            {
                SessionId = "1234",
                ProductId = 1,
                Quantity = 1,
                Price = 120
            };

            cartService.AddtoCart(itemCartVm);

            return Redirect("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult One(int donuts = 1)
        {
            ViewBag.Donuts = donuts;
            return View();
            
        }

        public ActionResult Two()
        {
            return View();
        }

        public ActionResult Three()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AjaxMethod(string name)
        {
            PersonModel person = new PersonModel
            {
                Name = name,
                DateTime = DateTime.Now.ToString()
            };
            return Json(person);
        }

        [HttpPost]
        public JsonResult GetProductById(string productId)
        {
            var product = new Product()
            {
                Id = 1,
                Code = "Code",
                Description = "Descript"
                
            };
            return Json(product);
        }
    }

    public class PersonModel
    {
        ///<summary>
        /// Gets or sets Name.
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        /// Gets or sets DateTime.
        ///</summary>
        public string DateTime { get; set; }
    }
}