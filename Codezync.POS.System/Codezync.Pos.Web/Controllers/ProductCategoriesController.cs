﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Codezync.Pos.Web.Controllers
{
    public class ProductCategoriesController : AdminController
    {
        private ProductCategoryService productService;
        private string BASE_URL = ConfigurationManager.AppSettings["BaseUrl"];

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            productService = new ProductCategoryService(CZDBContext);
        }
        // GET: ProductCategories
        public ActionResult Index()
        {
            ViewBag.Action = "Create";

            return View();
        }
        public PartialViewResult List(int page = 1)
        {
            var cats = productService.GetProductCategories("",page);
            return PartialView("_ProductCategoryList", cats);
        }
        public ActionResult Create()
        {
            ViewBag.Action = "Create";
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProductCategoryViewModel vm, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (productService.Exists(vm.Code))
                    {
                        ModelState.AddModelError("Code", "Product Category already exists");
                        return View("Index", vm);

                    }
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/ProductCategory/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/ProductCategory/")
                                                              + file.FileName);
                        vm.CategoryImgeUrl = url;
                    }
                    productService.Add(vm);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);
                }
            }

            return View("Index", vm);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productCat = productService.GetProductCategoryById(id.Value);
            if (productCat == null)
            {
                return HttpNotFound();
            }
            ViewBag.Action = "Edit";
            return View("Index", productCat);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProductCategoryViewModel vm, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/ProductCategory/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/ProductCategory/")
                                                              + file.FileName);
                        vm.CategoryImgeUrl = url;
                    }

                    productService.Update(vm);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);

                }
            }
            ViewBag.Action = "Edit";
            return View("Index");
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cat = productService.GetProductCategoryById(id.Value);
            if (cat == null)
            {
                return HttpNotFound();
            }

            ViewBag.Action = "Delete";
            return View("Index", cat);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    productService.Delete(id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);

                }
            }
            return View("Index");
        }


    }
}