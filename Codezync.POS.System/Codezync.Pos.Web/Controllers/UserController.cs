﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Codezync.Pos.Data.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;

namespace Codezync.Pos.Web.Controllers
{
    public class UserController : BaseController
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        UserService userService = new UserService();
        public UserController()
        {
        }

        public UserController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }


            var userRoles = await UserManager.GetRolesAsync(user.Id);

            return View(new EditUserViewModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                FullName = user.FullName,
                RolesList = RoleManager.Roles.ToList().Select(x => new RolesViewModel()
                {
                    Name = x.Name
                }),
                UserRoles = userRoles.Select(x => new RolesViewModel()
                {
                    IsSelected = true,
                    Name = x
                })


            });
        }
       
        public ViewResult Index(UserFilterCriteria criteria = null, int page = 1)
        {
            PrepareSelectLists(criteria);
            ViewBag.SearchString = criteria.SearchString;

            var users = userService.GetUsersList(criteria, page);
            return View(users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditUserViewModel editUser, PostedRoles postedRoles)
        {
            string userId = string.Empty;
            IList<string> userRoles = null;
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                userId = user.Id;
                user.UserName = editUser.UserName;
                user.Email = editUser.Email;
                user.FullName = editUser.FullName;
                user.IsDeleted = false;
                userRoles = await UserManager.GetRolesAsync(user.Id);

                var selectedRole = postedRoles.RoleList ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("An Error occured", result.Errors.First());
                    return View(editUser);
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("An Error occured", result.Errors.First());
                    return View(editUser);
                }
                return RedirectToAction("Index");

            }
            userRoles = userRoles ?? await UserManager.GetRolesAsync(userId);
            editUser.UserRoles = userRoles.Select(x => new RolesViewModel()
            {
                IsSelected = true,
                Name = x
            });
            editUser.RolesList = RoleManager.Roles.ToList().Select(x => new RolesViewModel()
            {
                Name = x.Name
            });

            return View(editUser);
        }


        private void PrepareSelectLists(UserFilterCriteria criteria)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "FullName", Text = "Full Name" });
            items.Add(new SelectListItem() { Value = "username", Text = "User Name" });
            items.Add(new SelectListItem() { Value = "email", Text = "Email" });

            var searchByList = new SelectList(items, "Value", "Text", criteria.SearchBy);
            ViewBag.SearchBy = searchByList;


        }

    }
}