﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Codezync.Pos.Web.Controllers
{
    public class CustomerController : BaseController
    {
        private CustomerService customerService;

        private string BASE_URL = ConfigurationManager.AppSettings["BaseUrl"];

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            customerService = new CustomerService(CZDBContext);

        }
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ViewCustomer(int customerId = 0)
        {
            var _Customer = customerService.GetCustomerById(customerId);
            if (_Customer != null)
                return PartialView("_ViewCustomer", _Customer);
            return PartialView("_ViewCustomer", new CustomerViewModel());

        }

        public PartialViewResult CustomerList(string searchKey, int page = 1)
        {
            var _CustomerList = customerService.GetCustomersList(searchKey, page);
            return PartialView("_CustomerList", _CustomerList);
        }

        [HttpGet]
        //CreateCustomer
        public PartialViewResult CreateCustomer()
        {
            
            var _Customer = new CustomerViewModel();
           
            return PartialView("_CreateCustomer", _Customer);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCustomer(CustomerViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/customerImages/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/customerImages/")
                                                              + file.FileName);
                        model.CustomerImgeUrl = url;
                    }
                    
                    customerService.addCustomer(model);
                    return Redirect("Index");

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }
         return View();

        }

        public PartialViewResult EditCustomer(int customerId = 0)
        {

            var _Customer = new CustomerViewModel();
            
            if (customerId != 0) {
                _Customer = customerService.GetCustomerById(customerId);
            }
            return PartialView("_EditCustomer", _Customer);

        }
        public async Task<ActionResult> SetCustomer(int customerId = 0)
        {
            if (Session["CART_ID"] != null)
            {
                customerService.setCustomer(customerId, Session["CART_ID"].ToString());
            }
            
            return RedirectToAction("Index","POS");

        }
        public async Task<ActionResult> DeleteCustomer(int customerId = 0)
        {
            
            customerService.deleteCustomer(customerId);
            return Redirect("Index");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCustomer(CustomerViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/customerImages/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/customerImages/")
                                                              + file.FileName);
                        model.CustomerImgeUrl = url;
                    }

                    customerService.editCustomer(model);
                    return Redirect("Index");

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }
            return View();

        }


    }
}