﻿using Codezync.Pos.Data.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Codezync.Pos.Web.Controllers
{
    public class RoleController : AdminController
    {
        public RoleController()
        {
        }

        public RoleController(ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }



        public ActionResult Index()
        {
            ViewBag.Action = "Create";
            return View(new RoleViewModel());
        }

        public PartialViewResult List()
        {
            var roles = RoleManager.Roles;
            var model = roles.Select(r => new RoleViewModel() { Id = r.Id, Name = r.Name, NumberOfUsers = r.Users.Count });
            return PartialView("_RoleList", model.OrderBy(m => m.Name));
        }

        //
        // GET: /Roles/Create
        public ActionResult Create()
        {
            ViewBag.Action = "Create";
            return View("Index");
        }

        //
        // POST: /Roles/Create
        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole(roleViewModel.Name);
                var roleresult = await RoleManager.CreateAsync(role);
                if (!roleresult.Succeeded)
                {
                    ModelState.AddModelError("Name", roleresult.Errors.First());
                    return View("Index", roleViewModel);
                }
                return RedirectToAction("Index");
            }
            return View("Index", roleViewModel);
        }

        //
        // GET: /Roles/Edit/Admin
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            RoleViewModel roleModel = new RoleViewModel { Id = role.Id, Name = role.Name };
            ViewBag.Action = "Edit";
            return View("Index", roleModel);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]

        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Name,Id")] RoleViewModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var role = await RoleManager.FindByIdAsync(roleModel.Id);
                role.Name = roleModel.Name;
                var result = await RoleManager.UpdateAsync(role);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("Name", result.Errors.First());
                    ViewBag.Action = "Edit";
                    return View("Index", roleModel);
                }
                return RedirectToAction("Index");
            }
            ViewBag.Action = "Edit";
            return View("Index", roleModel);
        }

        //
        // GET: /Roles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            var model = new RoleViewModel { Id = role.Id, Name = role.Name };
            ViewBag.Action = "Delete";
            return View("Index", model);
        }

        //
        // POST: /Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var role = await RoleManager.FindByIdAsync(id);
                if (role == null)
                {
                    return HttpNotFound();
                }
                var result = await RoleManager.DeleteAsync(role);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    ViewBag.Action = "Delete";
                    return View();
                }
                return RedirectToAction("Index");
            }
            ViewBag.Action = "Delete";
            return View("index");
        }
    }
}