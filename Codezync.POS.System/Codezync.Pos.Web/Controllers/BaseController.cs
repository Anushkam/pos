﻿using Codezync.Pos.Data.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Codezync.Pos.Web.Controllers
{
    public class BaseController : Controller
    {
        public ApplicationDbContext CZDBContext { get; set; }
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            //Get the single context from OWIN pipe line
            CZDBContext = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
        }
    }
#if !DEBUG
     [Authorize(Roles = "Administrator")]
#endif
    public class AdminController : BaseController
    {

    }
}