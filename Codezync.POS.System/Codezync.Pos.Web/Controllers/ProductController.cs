﻿using Codezync.Pos.Data.Service;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Codezync.Pos.Web.Controllers
{
    public class ProductController : BaseController
    {
        private ProductService productService;
        private ProductCategoryService productCatService;
        private string BASE_URL = ConfigurationManager.AppSettings["BaseUrl"];


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            productService = new ProductService(CZDBContext);
            productCatService = new ProductCategoryService(CZDBContext);
        }

        public ProductController()
        {
        }
        // GET: Product
        public ActionResult Index(ProductCriteria criteria, int page = 1)
        {
            PrepareSelectLists(criteria);
            ViewBag.SearchString = criteria.SearchString;
            ViewBag.SearchByString = criteria.SearchBy;
            ViewBag.ProductCategory = criteria.ProductCategoryId;

            return View();
        }
        public ActionResult Create()
        {
            var categories = productCatService.GetProductCategories();
            ViewBag.Categories = new MultiSelectList(categories, "Id", "Name");
            var Units = productCatService.GetUnits();
            ViewBag.Units = new SelectList(Units, "Id", "Name");
            var product = new ProductViewModel();
            return View(product);
        }
        public PartialViewResult List(ProductCriteria criteria, int page = 1)
        {
            PrepareSelectLists(criteria);
            ViewBag.SearchString = criteria.SearchString;
            ViewBag.SearchByString = criteria.SearchBy;
            ViewBag.ProductCategory = criteria.ProductCategoryId;


            var products = productService.GetProdctsAsPagedList(criteria, page);
            return PartialView("_ProductList", products);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProductViewModel vm, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/Products/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/Products/")
                                                              + file.FileName);
                        vm.ImageUrl = url;
                    }

                    productService.Add(vm);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);
                }
            }
            var categories = productCatService.GetProductCategories();
            ViewBag.Categories = new MultiSelectList(categories, "Id", "Name");
            return View( vm);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = productService.GetProductById((int)id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.Action = "Edit";
            var categories = productCatService.GetProductCategories();
            var selectedIds = model.ProductCategories.Select(c=> c.Id).ToArray();
            ViewBag.Categories = new MultiSelectList(categories, "Id", "Name", selectedIds);
            var Units = productCatService.GetUnits();
            ViewBag.Units = new SelectList(Units, "Id", "Name");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProductViewModel vm, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        var url = $"{BASE_URL}/Content/Products/{file.FileName}";
                        file.SaveAs(HttpContext.Server.MapPath("~/Content/Products/")
                                                              + file.FileName);
                        vm.ImageUrl = url;
                    }
                    productService.UpdateProduct(vm);
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);

                }
            }
            ViewBag.Action = "Edit";
            var categories = productCatService.GetProductCategories();
            var selectedIds = vm.ProductCategories.Select(c => c.Id).ToArray();
            ViewBag.Categories = new MultiSelectList(categories, "Id", "Name", selectedIds);
            return View(vm);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = productService.GetProductById((int)id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.Action = "Delete";
            var categories = productCatService.GetProductCategories();
            var selectedIds = product.ProductCategories.Select(c => c.Id).ToArray();
            ViewBag.Categories = new MultiSelectList(categories, "Id", "Name", selectedIds);
            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    productService.DeleteProduct(id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex);

                }
            }
            return View();
        }
        private void PrepareSelectLists(ProductCriteria criteria)
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "Name", Text = "Name" });
            items.Add(new SelectListItem() { Value = "Code", Text = "Code" });

            var searchByList = new SelectList(items, "Value", "Text", criteria.SearchBy);
            ViewBag.SearchBy = searchByList;

            var categoryList = new SelectList(productCatService.GetProductCategories(), "Id", "Name", criteria.ProductCategoryId);
            ViewBag.ProductCategoryId = categoryList;


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //productCatService.Dispose(true);
            }
            base.Dispose(disposing);
        }
    }
}