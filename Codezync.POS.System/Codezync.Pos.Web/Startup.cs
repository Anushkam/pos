﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Codezync.Pos.Web.Startup))]
namespace Codezync.Pos.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
