﻿using Codezync.Pos.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Service
{
    public class AbstractService
    {
        public ApplicationDbContext Context { get; set; }
        protected AbstractService(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}
