﻿using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PagedList;
using System.Data.Entity;
using Udio.Data.DataServcie.Sql.Infrastructure;

namespace Codezync.Pos.Data.Service
{
    public class ProductService : AbstractService
    {

        public ProductService(ApplicationDbContext context) : base(context)
        {
        }

        private IQueryable<Product> GetAllProducts()
        {
            return Context.Products.Include(o => o.ProductCategories);
        }

        public ProductViewModel GetProductByCode(string code, bool isDeleted = false, bool IsActive = true)
        {
            var product = GetAllProducts().Where(p => p.Code == code && p.IsDeleted == isDeleted && p.IsActive == IsActive).FirstOrDefault();
            return Mapper.Map<ProductViewModel>(product);
        }
        public ProductViewModel GetProductById(int id, bool isDeleted = false, bool IsActive = true)
        {
            var product = GetAllProducts().Where(p => p.Id == id && p.IsDeleted == isDeleted).FirstOrDefault();
            return Mapper.Map<ProductViewModel>(product);
        }

        public IEnumerable<ProductViewModel> GetProdcts(string searchKey = "", int categoryId = 0)
        {
            var productList = GetAllProducts().Where(v => v.IsDeleted == false && v.IsActive == true);

            if (categoryId != 0)
            {
                productList = productList.Where(r => r.ProductCategories.Select(a => a.Id).Contains(categoryId));
                var t = productList.ToList();
            }

            if (!string.IsNullOrEmpty(searchKey))
            {
                productList = productList.Where(p => p.Name.Contains(searchKey) || p.Description.Contains(searchKey) || p.Code.Contains(searchKey));
            }


            var filteredProductList = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(productList);
            return filteredProductList;
        }

        public IPagedList<ProductViewModel> GetProdctsAsPagedList(ProductCriteria criteria = null, int page = 1)
        {
            var productList = GetAllProducts().Where(v => v.IsDeleted == false);
            int pageSize = 2;


            if (criteria != null)
            {
                productList = Filter(criteria, productList);
            }
            var filteredProductList = productList.OrderBy(u => u.Id).ToMappedPagedList<Product, ProductViewModel>(page, pageSize);

            return filteredProductList;
        }

        public IQueryable<Product> Filter(ProductCriteria criteria, IEnumerable<Product> products)
        {

            if (!string.IsNullOrEmpty(criteria.SearchString) && !string.IsNullOrEmpty(criteria.SearchBy))
            {
                switch (criteria.SearchBy.ToUpper())
                {
                    case "NAME":
                        products = products.Where(u => u.Name.ToLower().Contains(criteria.SearchString.ToLower()));
                        break;
                    case "CODE":
                        products = products.Where(u => u.Code.ToLower().Contains(criteria.SearchString.ToLower()));
                        break;
                }
            }
            if (criteria.ProductCategoryId != 0)
            {
                products = products.Where(p => p.ProductCategories.Any(a => a.Id == criteria.ProductCategoryId));
            }


            return products.AsQueryable();
        }
        /// <summary>
        /// delete product 
        /// </summary>
        /// <param name="id"></param>
        public void DeleteProduct(int id)
        {
            var productModel = Context.Products.Find(id);
            productModel.IsDeleted = true;
            productModel.UpdatedDate = DateTime.Now;
            Context.Entry(productModel).State = EntityState.Modified;
            Context.SaveChanges();
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="vm"></param>
        public void UpdateProduct(ProductViewModel vm)
        {
            //var existingProduct = Context.Products.Find(vm.Id);
            var updatedVM = Mapper.Map<Product>(vm);
            updatedVM.UpdatedDate = DateTime.Now;
            Context.Entry(updatedVM).State = EntityState.Modified;
            Context.SaveChanges();

        }
        public void Add(ProductViewModel vm)
        {
            try
            {
                
                if (vm.SelectedCategoryIds.Count() != 0)
                {
                    var catList = new List<ProductCategoryViewModel>();
                    foreach (var item in vm.SelectedCategoryIds) {

                        var cat = Context.ProductCategories.Where(c=>c.Id == item).FirstOrDefault();
                        catList.Add(Mapper.Map<ProductCategoryViewModel>(cat));
                    }

                    vm.ProductCategories = catList;

                }
                var model = Mapper.Map<Product>(vm);
                model.CreatedDate = DateTime.Now;
                model.IsDeleted = false;
                Context.Products.Add(model);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

            }

        }

    }
}
