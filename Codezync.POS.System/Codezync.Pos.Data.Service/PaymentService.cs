﻿using Codezync.Pos.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Service
{
    public class PaymentService : AbstractService
    {

        public PaymentService(ApplicationDbContext context) : base(context)
        {
        }

        public int AddCardPayment(string paymmentRef, double amount, int customerId)
        {
            var model = new CardPaymentReference()
            {
                Amount = amount,
                Date = DateTime.Now,
                CardReference = paymmentRef,
                CustomerId = customerId
            };
            Context.CardPaymentReferences.Add(model);
            Context.SaveChanges();

            return model.Id;
        }

        public int AddCashPayment(double paidAmount, double amount, int customerId)
        {
            var model = new CashPaymentReference()
            {
                Amount = amount,
                Date = DateTime.Now,
                Tendered = paidAmount,
                CustomerId = customerId
            };
            Context.CashPaymentReferences.Add(model);
            Context.SaveChanges();

            return model.Id;
        }
    }
}
