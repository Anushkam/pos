﻿using AutoMapper;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Udio.Data.DataServcie.Sql.Infrastructure;

namespace Codezync.Pos.Data.Service
{
    public class UserService : AbstractService
    {

        public UserService(ApplicationDbContext context) : base(context)
        {

        }

        
        public UserService() : base(new ApplicationDbContext())
        {
        }
        public IQueryable<ApplicationUser> GetUsers()
        {
            return Context.Users;
        }

        public IQueryable<string> GetUserRoles()
        {
            return Context.Roles.Select(s => s.Name);
        }
        

        public IPagedList<UserViewModel> GetUsersList(UserFilterCriteria criteria = null, int page = 1)
        {
            var pageSize = 25;
            var users = GetUsers().Include(u => u.Roles).Where(u => u.IsDeleted == false);
            var roles = Context.Roles;

            var selectedUsers = users.Select(u => new UserViewModel
            {
                Id = u.Id,
                UserName = u.UserName,
                FullName = u.FullName,
                Email = u.Email,
                ListOfRoles = roles.Where(r => u.Roles.Select(c => c.RoleId).Contains(r.Id)).Select(r => r.Name)
            }).AsEnumerable(); ;
            selectedUsers = Filter(criteria, selectedUsers);

            return selectedUsers.OrderByDescending(u => u.Id).ToMappedPagedList<UserViewModel, UserViewModel>(page, pageSize);
        }

        public IEnumerable<UserViewModel> Filter(UserFilterCriteria criteria, IEnumerable<UserViewModel> users)
        {

            if (criteria != null)
            {

                if (!string.IsNullOrEmpty(criteria.SearchString) && !string.IsNullOrEmpty(criteria.SearchBy))
                {
                    switch (criteria.SearchBy.ToUpper())
                    {
                        case "FULLNAME":
                            users = users.Where(u => u.FullName.ToLower().Contains(criteria.SearchString.ToLower()));
                            break;
                        case "EMAIL":
                            users = users.Where(u => u.Email.ToLower().Contains(criteria.SearchString.ToLower()));
                            break;
                        case "USERNAME":
                            users = users.Where(u => u.UserName.ToLower().Contains(criteria.SearchString.ToLower()));
                            break;

                    }
                }


            }

            return users;
        }

        
        public ApplicationUser GetUserById(string Id)
        {
            var user = Context.Users.AsNoTracking().FirstOrDefault(u => u.Id == Id);
            return user;
        }

        
        public void DeleteUser(string id)
        {
            var userModel = Context.Users.Find(id);
            userModel.IsDeleted = true;
            userModel.DeletedDate = DateTime.Now;
            Context.Entry(userModel).State = EntityState.Modified;
            Context.SaveChanges();
        }

    }
}
