﻿using AutoMapper;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Service
{


    public class CartService : AbstractService
    {

        public CartService(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Add items to cart
        /// </summary>
        /// <param name="vm"></param>
        public void AddtoCart(ItemCartViewModel vm)
        {
            var existingItems = Context.ItemCart.Where(i => i.ProductId == vm.ProductId && i.SessionId == vm.SessionId).FirstOrDefault();
            if (existingItems != null)
            {
                var priceOfOne = Context.Products.Where(p => p.Id == vm.ProductId).Select(e => e.Price).FirstOrDefault();
                existingItems.Quantity = existingItems.Quantity + 1;
                existingItems.Price = priceOfOne * existingItems.Quantity;
                Context.Entry(existingItems).State = EntityState.Modified;
                Context.SaveChanges();

            }
            else
            {

                var model = Mapper.Map<ItemCart>(vm);

                //get updated customer id
                var _cart = Context.ItemCart.FirstOrDefault();                
                var _CustomerId = 0;
                //get updated paymentRef id
                var _paymentRefId = 0;
                if (_cart != null)
                {
                    _CustomerId = _cart.CustomerId;

                    //get updated paymentRef id
                    if (_cart.PaymentType == PaymentType.Cash)
                    {
                        _paymentRefId = _cart.CashPaymentReferenceId.Value;
                        model.CashPaymentReferenceId = _paymentRefId;
                    }
                    else {
                        _paymentRefId = _cart.CardPaymentReferenceId.Value;
                        model.CardPaymentReferenceId = _paymentRefId;
                    }

                }
                else {
                    _CustomerId = Context.Customers.Where(c => c.Name.ToLower() == "cash").FirstOrDefault().Id;

                }
                
                model.Quantity = 1;
                model.CustomerId = _CustomerId;
                model.Price = Context.Products.Where(p => p.Id == vm.ProductId).Select(e => e.Price).FirstOrDefault();
                Context.ItemCart.Add(model);
                Context.SaveChanges();
            }
        }
        public void AddCashPayment(int CashPaymentReferenceId, double discountRate)
        {
            var cart = Context.ItemCart.AsQueryable().AsNoTracking().ToList();
            if (cart.Count() != 0) {
                foreach (var item in cart)
                {
                    item.CashPaymentReferenceId = CashPaymentReferenceId;
                    item.CardPaymentReferenceId = 0;
                    item.DiscountRate = discountRate;
                    item.PaymentType = PaymentType.Cash;
                    Context.Entry(item).State = EntityState.Modified;
                    Context.SaveChanges();
                }
            }
        }
        public void AddCardPayment(int CardPaymentReferenceId, double discountRate)
        {
            var cart = Context.ItemCart.AsQueryable().AsNoTracking().ToList();
            if (cart.Count() != 0)
            {
                foreach (var item in cart)
                {
                    item.CardPaymentReferenceId = CardPaymentReferenceId;
                    item.CashPaymentReferenceId = 0;
                    item.DiscountRate = discountRate;
                    item.PaymentType = PaymentType.Card;
                    Context.Entry(item).State = EntityState.Modified;
                    Context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Remove an item from cart
        /// </summary>
        /// <param name="productId"></param>
        public void RemoveItemFromCart(int productId)
        {
            var item = Context.ItemCart.Where(c => c.ProductId == productId).FirstOrDefault();

            if (item != null)
            {
                if (item.Quantity > 1)
                {
                    var priceOfOne = Context.Products.Where(p => p.Id == productId).Select(e => e.Price).FirstOrDefault();

                    item.Quantity = item.Quantity - 1;
                    item.Price = item.Price - priceOfOne;
                    Context.Entry(item).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                else
                {
                    Context.ItemCart.Remove(item);
                    Context.SaveChanges();
                }

            }

        }
        public CustomerViewModel GetSelectedCustomer(string sessionId)
        {
            var items = Context.ItemCart.AsQueryable();

            if (items.Count() != 0)
            {
                var customervm = Context.Customers.Where(c => c.Id == items.FirstOrDefault().CustomerId).FirstOrDefault();
                return Mapper.Map<CustomerViewModel>(customervm);
            }
            else
            {
                var customervm = Context.Customers.Where(c => c.Name.ToLower() == "cash").FirstOrDefault();
                return Mapper.Map<CustomerViewModel>(customervm);
            }
        }

        /// <summary>
        /// Get summery of the current cart
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public CartSummeryViewModel GetCartSummery(string sessionId = "")
        {
            var cart = Context.ItemCart.AsQueryable();
            if (!string.IsNullOrEmpty(sessionId))
            {
                cart = cart.Where(i => i.SessionId == sessionId);
            }

            if (cart.Count() == 0)
            {
                return new CartSummeryViewModel()
                {
                    SessionId = "",
                    TotalAfterDiscounts = 0.0,
                    TotalAmount = 0.0,
                    TotalQuantity = 0,
                    CustomerId = Context.Customers.Where(c=>c.Name.ToLower() == "cash").FirstOrDefault().Id
                };
            }
            var paymetType = Context.ItemCart.FirstOrDefault().PaymentType;
            var discountRate = Context.ItemCart.FirstOrDefault().DiscountRate;
            var summery =
                (from item in Context.ItemCart
                 where item.SessionId == sessionId
                 group item by item.SessionId
                into g
                 select new CartSummeryViewModel
                 {
                     TotalQuantity = g.Sum(x => x.Quantity),
                     TotalAmount = g.Sum(t => t.Price),
                     CustomerId = Context.ItemCart.FirstOrDefault().CustomerId,
                     PaymentType = paymetType,
                     TotalAfterDiscounts = g.Sum(t => t.Price) - (g.Sum(t => t.Price) * discountRate)

                 });
            return summery.FirstOrDefault();
        }

        /// <summary>
        /// Clear cart
        /// </summary>
        /// <param name="sessionId"></param>
        public void ClearCart(string sessionId)
        {
            var items = Context.ItemCart;
            Context.ItemCart.RemoveRange(items);
            Context.SaveChanges();
        }

        /// <summary>
        /// Get items in the current cart
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public IEnumerable<ItemCartViewModel> GetCartItems(string sessionId = "")
        {
            var items = Context.ItemCart.AsQueryable().Include(p => p.Product).Include(p => p.Customer);
            if (!string.IsNullOrEmpty(sessionId))
            {
                items = items.Where(v => v.SessionId == sessionId);

            }
            var itemsVm = Mapper.Map<IEnumerable<ItemCart>, IEnumerable<ItemCartViewModel>>(items);
            return itemsVm;
        }

        public PaymentType GetCurrentPaymentType(string sessionId = "")
        {
            var items = Context.ItemCart.AsQueryable().Include(p => p.Product).Include(p => p.Customer);
            if (!string.IsNullOrEmpty(sessionId))
            {
                items = items.Where(v => v.SessionId == sessionId);

            }

            if (items.Count() != 0)
            {
                return items.FirstOrDefault().PaymentType;
            }

            return PaymentType.Cash;
        }

    }
}
