﻿using AutoMapper;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Service
{
    public class OrderService : AbstractService
    {
        private CartService cartService;
        public OrderService(ApplicationDbContext context) : base(context)
        {
            cartService = new CartService(context);
        }

        /// <summary>
        /// get all orders in the db
        /// </summary>
        /// <returns></returns>
        private IQueryable<Order> GetAllOrders()
        {
            return Context.Orders.AsQueryable().Include(o => o.OrderItems);
        }

        /// <summary>
        /// get order by id
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        public OrderViewModel GetOrderById(int orderID)
        {
            var order = GetAllOrders().Where(p => p.Id == orderID).FirstOrDefault();
            return Mapper.Map<OrderViewModel>(order);
        }

        /// <summary>
        /// get summery of invoice for current session
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        public InvoiceSumeryViewModel GetInvoiceSummery(int orderID)
        {
            var order = GetAllOrders().Include(c => c.Customer).Where(p => p.Id == orderID).FirstOrDefault();
            var totalWithDiscounts = 0.0;
            if (order.DiscountRate != 0)
            {
                totalWithDiscounts = order.Amount - (order.Amount * (order.DiscountRate/100));

            }
            var summery = new InvoiceSumeryViewModel()
            {
                OrderId = order.Id,
                Customer = Mapper.Map<CustomerViewModel>(order.Customer),
                Date = order.CreatedDate,
                DiscountRate = order.DiscountRate,
                Paid = order.AmountPaid,
                SubTotal = order.Amount,
                TotalAmount = totalWithDiscounts,
                OrderedItems = GetOrderItemsByOrderId(orderID)

            };
            return summery;
        }

        /// <summary>
        /// Save an order and fill order items from cart
        /// </summary>
        /// <param name="CashierId"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public int SaveAnOrder(string CashierId,string sessionId = "")
        {

            var summery = cartService.GetCartSummery(sessionId);
            var AmountPaid = 0.0;
            var Amount = 0.0;

            if (summery.PaymentType == PaymentType.Card)
            {
                var id = Context.ItemCart.FirstOrDefault().CardPaymentReferenceId;
                var pay = Context.CardPaymentReferences.Where(i => i.Id == id);
                AmountPaid = pay.FirstOrDefault().Amount;
                Amount = pay.FirstOrDefault().Amount;
            }
            else
            {
                var id = Context.ItemCart.FirstOrDefault().CashPaymentReferenceId;
                var pay = Context.CashPaymentReferences.Where(i => i.Id == id);
                AmountPaid = pay.FirstOrDefault().Tendered;
                Amount = pay.FirstOrDefault().Amount;
            }

            //set order view model
            var vm = new OrderViewModel()
            {
                PaymentType = summery.PaymentType,
                CashierId = CashierId,
                CreatedDate = DateTime.Now,
                CustomerId = Context.ItemCart.FirstOrDefault().CustomerId,
                AmountPaid = AmountPaid,
                DiscountRate = Context.ItemCart.FirstOrDefault().DiscountRate.Value,
                Amount = Amount,
                TotalWithDiscounts = Amount

            };


            //Save order
            var cartItems = cartService.GetCartItems(sessionId);
            var orderId = 0;
            if (vm != null)
            {
                var order = Mapper.Map<Order>(vm);
                order.CreatedDate = DateTime.Now;
                Context.Orders.Add(order);
                Context.SaveChanges();
                orderId = order.Id;
            }
            //Add items from cart to order items table
            if (cartItems.Count() != 0)
            {
                foreach (var item in cartItems)
                {
                    var orderItemsVm = new OrderItemViewModel()
                    {
                        OrderId = orderId,
                        Price = item.Price,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity
                    };
                    AddOrderItem(orderItemsVm);
                }
            }

            return orderId;

        }

        /// <summary>
        /// add a single order item
        /// </summary>
        /// <param name="vm"></param>
        public void AddOrderItem(OrderItemViewModel vm)
        {
            if (vm != null)
            {
                var order = Mapper.Map<OrderItem>(vm);
                Context.OrderItems.Add(order);
                Context.SaveChanges();
            }
        }


        /// <summary>
        /// get all order items for an order
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        public IEnumerable<OrderItemViewModel> GetOrderItemsByOrderId(int orderID)
        {
            var orderItems = Context.OrderItems.Include(o => o.Order).Include(o => o.Product)
                .Where(p => p.OrderId == orderID).AsEnumerable();
            return Mapper.Map<IEnumerable<OrderItem>, IEnumerable<OrderItemViewModel>>(orderItems);

        }



    }
}
