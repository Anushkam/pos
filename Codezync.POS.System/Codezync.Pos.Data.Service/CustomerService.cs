﻿using AutoMapper;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Udio.Data.DataServcie.Sql.Infrastructure;

namespace Codezync.Pos.Data.Service
{
    public class CustomerService : AbstractService
    {
        public CustomerService(ApplicationDbContext context) : base(context)
        {
        }

        private IQueryable<Customer> GetAllCustomers()
        {
            return Context.Customers.Where(c=>c.IsDeleted == false);
        }

        public CustomerViewModel GetCustomerById(int id)
        {
            var customer = GetAllCustomers().Where(p => p.Id == id).FirstOrDefault();
            return Mapper.Map<CustomerViewModel>(customer);
        }

        public IPagedList<CustomerViewModel> GetCustomersList(String criteria = null, int page = 1)
        {
            var pageSize = 5;
            var customers = GetAllCustomers();

            if (criteria != null) {

                customers = customers.Where(c=>c.Name.ToLower().Contains(criteria.ToLower()));
                customers = customers.Where(c => c.Email.ToLower().Contains(criteria.ToLower()));
                customers = customers.Where(c => c.Phone.Contains(criteria));
            }

            return customers.OrderBy(u => u.Id).ToMappedPagedList<Customer, CustomerViewModel>(page, pageSize);
        }

        /// <summary>
        /// add new customer 
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public int addCustomer(CustomerViewModel vm)
        {
            if (vm != null) {
                vm.CreatedDate = DateTime.Now;
                vm.ModifiedDate = null;
                vm.IsDeleted = false;
                var newCustomer = Mapper.Map<Customer>(vm);

                Context.Customers.Add(newCustomer);
                Context.SaveChanges();
                return newCustomer.Id;
            }

            return 0;
        }

        /// <summary>
        /// update customer details
        /// </summary>
        /// <param name="vm"></param>
        public void editCustomer(CustomerViewModel vm)
        {
            if (vm != null)
            {
                var existCustomer = Context.Customers.Where(c=>c.Id == vm.Id).AsNoTracking().FirstOrDefault();
                existCustomer.ModifiedDate = DateTime.Now;
                existCustomer.IsDeleted = false;
                var editingCustomer = Mapper.Map<Customer>(vm);

                Context.Entry(editingCustomer).State = EntityState.Modified;
                Context.SaveChanges();
            }
            
        }

        /// <summary>
        /// update custommer in the cart entry table
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="sessionId"></param>
        public void setCustomer(int customerId, string sessionId)
        {
            var existItems = Context.ItemCart.Where(c => c.SessionId == sessionId).AsNoTracking().ToList();
            
            if (existItems != null)
            {
                foreach (var item in existItems)
                {
                    item.CustomerId = customerId;
                    Context.Entry(item).State = EntityState.Modified;
                    Context.SaveChanges();
                }
            }


        }

        /// <summary>
        /// deleting a custmer by id
        /// </summary>
        /// <param name="id"></param>
        public void deleteCustomer(int id)
        {
            if (id != 0)
            {
                var existCustomer = Context.Customers.Where(c => c.Id ==  id).AsNoTracking().FirstOrDefault();
                existCustomer.ModifiedDate = DateTime.Now;
                existCustomer.IsDeleted = true;

                Context.Entry(existCustomer).State = EntityState.Modified;
                Context.SaveChanges();
            }

        }
    }
}
