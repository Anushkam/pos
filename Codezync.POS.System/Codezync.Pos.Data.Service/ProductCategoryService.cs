﻿using AutoMapper;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Udio.Data.DataServcie.Sql.Infrastructure;

namespace Codezync.Pos.Data.Service
{
    public class ProductCategoryService:AbstractService
    {
        public ProductCategoryService(ApplicationDbContext context) : base(context)
        {
        }

        private IQueryable<ProductCategory> GetAllProductCategories()
        {
            return Context.ProductCategories.AsQueryable().Include(d=>d.Products);
        }
       

        public ProductCategoryViewModel GetProductCategoryByCode(string code, bool isDeleted = false, bool IsActive = true)
        {
            var productCategory = GetAllProductCategories().Where(p => p.Code == code && p.IsDeleted == isDeleted
            && p.IsActive == IsActive ).FirstOrDefault();
            return Mapper.Map<ProductCategoryViewModel>(productCategory);
        }

        public bool Exists(string code, bool isDeleted = false)
        {

            return Context.ProductCategories.Any(c => c.Code.ToLower() == code.ToLower() && c.IsDeleted == isDeleted);
        }

        public IEnumerable<ProductViewModel> GetProductsByCategoryId(int id= 0, bool isDeleted = false, bool IsActive = true)
        {
            if (id != 0)
            {
                var productCategory = GetAllProductCategories().Where(p => p.Id == id && p.IsDeleted == isDeleted
                     && p.IsActive == IsActive).FirstOrDefault().Products;
                return Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(productCategory);

            }

            var products = Context.Products.Where(p => p.IsDeleted == false && p.IsActive == true);
            return Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

        }
        public IEnumerable<UnitViewModel> GetUnits()
        {
            

            var units = Context.Units;
            return Mapper.Map<IEnumerable<Unit>, IEnumerable<UnitViewModel>>(units);

        }
        public ProductCategoryViewModel GetProductCategoryById(int id, bool isDeleted = false)
        {
            var productCategory = GetProductCategories().Where(p => p.Id == id 
            && p.IsDeleted == isDeleted).FirstOrDefault();
            return Mapper.Map<ProductCategoryViewModel>(productCategory);
        }
        public void Add(ProductCategoryViewModel model)
        {

            var category = Mapper.Map<ProductCategory>(model);
            category.CreatedDate = DateTime.Now;
            Context.ProductCategories.Add(category);
            Context.SaveChanges();

        }

        public void Update(ProductCategoryViewModel vm)
        {
            var updatedVM = Mapper.Map<ProductCategory>(vm);
            updatedVM.UpdatedDate = DateTime.Now;
            Context.Entry(updatedVM).State = EntityState.Modified;
            Context.SaveChanges();

        }

        public void Delete(int id)
        {
            var category = Context.ProductCategories.Find(id);
            category.IsDeleted = true;
            category.UpdatedDate = DateTime.Now;
            Context.Entry(category).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IPagedList<ProductCategoryViewModel> GetProductCategories(string searchKey = "", int page = 1)
        {
            int pageSize = 10;
            var productList = Context.ProductCategories.Where(v => v.IsDeleted == false );
            
            if (!string.IsNullOrEmpty(searchKey))
            {
                productList = productList.Where(p => p.Name.Contains(searchKey) || p.Description.Contains(searchKey) || p.Code.Contains(searchKey));
            }

            var filteredCategoryList = productList.OrderBy(u => u.Id).ToMappedPagedList<ProductCategory, ProductCategoryViewModel>(page, pageSize);

            return filteredCategoryList;
        }

        //public IEnumerable<ProductViewModel> GetProdcts(string searchKey = "", int categoryId = 0)
        //{
        //    var productList = GetAllProducts().Where(v => v.IsDeleted == false && v.IsActive == true);

        //    if (string.IsNullOrEmpty(searchKey))
        //    {
        //        productList = productList.Where(p => p.Name.Contains(searchKey) || p.Description.Contains(searchKey) || p.Code.Contains(searchKey));
        //    }
        //    //if (categoryId != 0)
        //    //{
        //    //    productList = productList.Where(c => c.ProductCategories.id)
        //    //}

        //    var filteredProductList = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(productList);
        //    return filteredProductList;
        //}
    }
}
