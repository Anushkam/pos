﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Udio.Data.DataServcie.Sql.Infrastructure
{
    public static class Converters
    {
        public static IPagedList<TDestination> ToMappedPagedList<TSource, TDestination>(this IEnumerable<TSource> list, int page, int pageSize)
        {
            IPagedList<TSource> pagedList = list.ToPagedList(page, pageSize);
            IEnumerable<TDestination> sourceList = Mapper.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(pagedList);
            IPagedList<TDestination> pagedResult = new StaticPagedList<TDestination>(sourceList, pagedList.GetMetaData());
            return pagedResult;
        }
    }
}
