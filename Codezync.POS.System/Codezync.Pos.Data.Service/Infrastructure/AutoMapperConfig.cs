﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codezync.Pos.Data.Models;
using Codezync.Pos.Data.ViewModels;

namespace Codezync.Pos.Data.Service
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                //Configs
                cfg.CreateMap<Configuration, ConfigurationViewModel>().ReverseMap();

                //products
                cfg.CreateMap<Product, ProductViewModel>().ReverseMap();

                //products
                cfg.CreateMap<ApplicationUser, UserViewModel>().ReverseMap();
                cfg.CreateMap<ApplicationUser, EditUserViewModel>().ReverseMap();

                //product categories
                cfg.CreateMap<ProductCategory, ProductCategoryViewModel>().ReverseMap();

                //UnitViewModel
                cfg.CreateMap<Unit, UnitViewModel>().ReverseMap();

                //Order
                cfg.CreateMap<OrderItem, OrderItemViewModel>().ReverseMap();
                cfg.CreateMap<Order, OrderViewModel>().ReverseMap();
                //Cart
                cfg.CreateMap<ItemCart, ItemCartViewModel>().ReverseMap();
                //PaymentType
                cfg.CreateMap<PaymentType, PaymentTypeViewModel>().ReverseMap();

                //customer
                cfg.CreateMap<Customer, CustomerViewModel>().ReverseMap();



            });
        }
    }

}
