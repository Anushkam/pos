﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Codezync.Pos.Data.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("PosSystemDB", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public IDbSet<IdentityUserLogin> UserLogins { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<CardPaymentReference> CardPaymentReferences { get; set; }
        public DbSet<Product> Products { get; set; }        

        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<ItemCart> ItemCart { get; set; }
        public DbSet<CashPaymentReference> CashPaymentReferences { get; set; }

        

    }
}
