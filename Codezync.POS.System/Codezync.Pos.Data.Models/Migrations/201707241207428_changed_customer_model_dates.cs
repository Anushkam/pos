namespace Codezync.Pos.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_customer_model_dates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Customers", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.Customers", "ModifiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "ModifiedDate");
            DropColumn("dbo.Customers", "CreatedDate");
            DropColumn("dbo.Customers", "IsDeleted");
        }
    }
}
