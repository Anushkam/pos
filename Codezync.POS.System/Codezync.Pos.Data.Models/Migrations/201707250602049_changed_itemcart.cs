namespace Codezync.Pos.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_itemcart : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashPaymentReferences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Double(nullable: false),
                        Tendered = c.Double(nullable: false),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            AddColumn("dbo.ItemCarts", "CashPaymentReferenceId", c => c.Int());
            AddColumn("dbo.ItemCarts", "CardPaymentReferenceId", c => c.Int());
            CreateIndex("dbo.ItemCarts", "CashPaymentReferenceId");
            CreateIndex("dbo.ItemCarts", "CardPaymentReferenceId");
            AddForeignKey("dbo.ItemCarts", "CardPaymentReferenceId", "dbo.CardPaymentReferences", "Id");
            AddForeignKey("dbo.ItemCarts", "CashPaymentReferenceId", "dbo.CashPaymentReferences", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ItemCarts", "CashPaymentReferenceId", "dbo.CashPaymentReferences");
            DropForeignKey("dbo.CashPaymentReferences", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.ItemCarts", "CardPaymentReferenceId", "dbo.CardPaymentReferences");
            DropIndex("dbo.CashPaymentReferences", new[] { "CustomerId" });
            DropIndex("dbo.ItemCarts", new[] { "CardPaymentReferenceId" });
            DropIndex("dbo.ItemCarts", new[] { "CashPaymentReferenceId" });
            DropColumn("dbo.ItemCarts", "CardPaymentReferenceId");
            DropColumn("dbo.ItemCarts", "CashPaymentReferenceId");
            DropTable("dbo.CashPaymentReferences");
        }
    }
}
