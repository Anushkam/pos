namespace Codezync.Pos.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_itemcart_discounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemCarts", "DiscountRate", c => c.Double());
            AlterColumn("dbo.Orders", "Amount", c => c.Double(nullable: false));
            AlterColumn("dbo.Orders", "DiscountRate", c => c.Double(nullable: false));
            AlterColumn("dbo.Orders", "TotalWithDiscounts", c => c.Double(nullable: false));
            AlterColumn("dbo.Orders", "AmountPaid", c => c.Double(nullable: false));
            AlterColumn("dbo.Orders", "Cost", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "AmountPaid", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "TotalWithDiscounts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "DiscountRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Orders", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ItemCarts", "DiscountRate");
        }
    }
}
