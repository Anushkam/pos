namespace Codezync.Pos.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_itemcart_paymentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CardPaymentReferences", "CardReference", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CardPaymentReferences", "CardReference");
        }
    }
}
