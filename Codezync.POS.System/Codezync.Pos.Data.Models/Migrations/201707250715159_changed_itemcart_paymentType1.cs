namespace Codezync.Pos.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_itemcart_paymentType1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ItemCarts", "CardPaymentReferenceId", "dbo.CardPaymentReferences");
            DropForeignKey("dbo.ItemCarts", "CashPaymentReferenceId", "dbo.CashPaymentReferences");
            DropIndex("dbo.ItemCarts", new[] { "CashPaymentReferenceId" });
            DropIndex("dbo.ItemCarts", new[] { "CardPaymentReferenceId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.ItemCarts", "CardPaymentReferenceId");
            CreateIndex("dbo.ItemCarts", "CashPaymentReferenceId");
            AddForeignKey("dbo.ItemCarts", "CashPaymentReferenceId", "dbo.CashPaymentReferences", "Id");
            AddForeignKey("dbo.ItemCarts", "CardPaymentReferenceId", "dbo.CardPaymentReferences", "Id");
        }
    }
}
