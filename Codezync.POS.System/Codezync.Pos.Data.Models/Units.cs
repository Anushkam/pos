﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Codezync.Pos.Data.Models
{
    [Table("Units")]
    public class Unit
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
    }
}