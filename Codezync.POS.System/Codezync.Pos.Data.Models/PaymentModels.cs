﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Models
{

    //[Table("PaymentTypes")]
    //public class PaymentType
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int Id { get; set; }
    //    [Required]
    //    [MaxLength(20)]
    //    public string Code { get; set; }
    //    [MaxLength(20)]
    //    public string Description { get; set; }

    //}

    public enum PaymentType
    {
        Cash = 0,
        Card = 1
    }

    public class CardPaymentReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string TransactionId { get; set; }
        public string CardReference { get; set; }
        public double Amount { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }

    public class CashPaymentReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public double Tendered { get; set; }
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }


}
