﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codezync.Pos.Data.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]       
        [ForeignKey("Cashier")]
        public string CashierId { get; set; }
        public ApplicationUser Cashier { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
       
        public PaymentType PaymentType { get; set; }

        public double Amount { get; set; }
        public double DiscountRate { get; set; }
        public double TotalWithDiscounts { get; set; }
        public double AmountPaid { get; set; }
        public double Cost { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public DateTime CreatedDate { get; set; }



    }

    [Table("OrderItems")]
    public class OrderItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public Order Order { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        

    }
}
